<?php

namespace Riverstone\MultiVendor\Api\Data;

interface SellerRegisterInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ID = 'id';
    const FIRST_NAME = 'first_name';

   /**
    * Get id.
    *
    * @return int
    */
    public function getId();

   /**
    * Set id.
    */
    public function setId($id);

   /**
    * Get DhlPostcode.
    *
    * @return varchar
    */
    public function getFirstName();

   /**
    * Set DhlPostcode.
    */
    public function setFirstName($firstName);

   

}
