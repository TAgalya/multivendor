<?php
namespace Riverstone\MultiVendor\Model;

use Magento\Framework\App\TemplateTypesInterface;
// use Magento\Framework\Mail\Template\TransportBuilder;
use Riverstone\MultiVendor\Model\Mail\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Mail\Template\TransportBuilderByStore;
use Magento\Store\Model\StoreManagerInterface;

class EmailSender
{

    protected const SELLER_REGISTER_SENDER = 'multi_vendor_section/sell_page/seller_register_sender';
    protected const SELLER_REGISTER_RECEIVER = 'multi_vendor_section/sell_page/seller_register_receiver';
    protected const SELLER_REGISTER_CC = 'multi_vendor_section/sell_page/seller_register_cc';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    private $transportBuilder;
    private $inlineTranslation;
    private $storeManager;

    public function __construct(
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    /*
     * get Current store id
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /*
     * get Current store Info
     */
    public function getStore()
    {
        return $this->storeManager->getStore();
    }

    public function sendEmail($data, $identifier)
    {

        $storeId = $this->getStoreId();

        $templateOptions = [
            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
            'store' => $storeId
        ];

        if($identifier == 'seller_approved_email_template'){
            $templateVars = [
                'first_name'    => $data['first_name'],
                'last_name'     => $data['last_name'],
                'email'         => $data['email'],
                'resetlink'         => $data['resetlink']
                ];
            $to = $data['email'];

        }

        if($identifier == 'seller_disapproved_email_template'){
            $templateVars = [
                'first_name'    => $data['first_name'],
                'last_name'     => $data['last_name'],
                'email'         => $data['email']
                ];
                $to = $data['email'];
        }

        if($identifier == 'seller_registration_email_template'){
            $templateVars = [
                'first_name'    => $data['first_name'],
                'last_name'     => $data['last_name'],
                'email'         => $data['email'],
                'shop_name'     => $data['shop_name'],
                'shop_url'      => $data['shop_url'],
                'phone'         => $data['phone'],
                'shop_description' => $data['shop_description'],
                'address'       => $data['address'],
                ];
                $to = $this->scopeConfig->getValue(self::SELLER_REGISTER_RECEIVER, ScopeInterface::SCOPE_STORE,$this->getStoreId());
        }
  
        
        $from = $this->scopeConfig->getValue(self::SELLER_REGISTER_SENDER, ScopeInterface::SCOPE_STORE,$this->getStoreId());
     
        $cc = $this->scopeConfig->getValue(self::SELLER_REGISTER_CC, ScopeInterface::SCOPE_STORE,$this->getStoreId());
     
        $this->inlineTranslation->suspend();

        $transport = $this->transportBuilder
            ->setTemplateIdentifier($identifier)
            ->setTemplateOptions($templateOptions)
            ->setTemplateVars($templateVars)
            ->setFrom($from)
            ->addTo($to)
            ->addCC($cc);

        if($identifier == 'seller_registration_email_template'){
            if(isset($data['logo_url'])){
                $this->transportBuilder->addAttachment(file_get_contents($data['logo_url']), $data['logo_name']);
            }

            if(isset($data['banner_url'])){
                $this->transportBuilder->addAttachment(file_get_contents($data['banner_url']),$data['banner_name']);  
            }
        }
        
        $transport = $transport->getTransport();
        $transport->sendMessage();

        $this->inlineTranslation->resume();

    }
}