<?php
namespace Riverstone\MultiVendor\Model\Config\Product;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Riverstone\MultiVendor\Model\SellerFactory;

class SellerList extends AbstractSource
{
    protected const APPROVED_SELLER = 'approved';
    protected $optionFactory;

    /**
     * Seller model factory
     *
     * @var \Riverstone\MultiVendor\Model\SellerFactory
     */
    protected $_sellerFactory;

    public function __construct(
        SellerFactory $sellerFactory
    ) {
        $this->_sellerFactory = $sellerFactory;
    }

    public function getAllOptions()
    {
        $sellerModel = $this->_sellerFactory->create();

        $sellerLists = $sellerModel->getCollection()
        ->addFieldToSelect('first_name')
        ->addFieldToSelect('last_name')
        ->addFieldToSelect('email')
        ->addFieldToSelect('shop_name')
        ->addFieldToSelect('id')
        ->addFieldToFilter('vendor_status', self::APPROVED_SELLER);

        $this->_options = [];

        foreach($sellerLists as $sellerList){
            $this->_options[] = ['label' => $sellerList->getFirstName().' '.$sellerList->getLastName(). ' - '. $sellerList->getShopName(), 'value' => $sellerList->getEmail()];
        }
    
        return $this->_options;
    }
}
