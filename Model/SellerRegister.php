<?php
namespace Riverstone\MultiVendor\Model;

use Riverstone\MultiVendor\Api\Data\SellerRegisterInterface;

class SellerRegister extends \Magento\Framework\Model\AbstractModel implements SellerRegisterInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'vendor_list';

    /**
     * @var string
     */
    protected $_cacheTag = 'vendor_list';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'vendor_list';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Riverstone\MultiVendor\Model\ResourceModel\SellerRegister');
    }
    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set Id.
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Get DC_POSTCODE.
     *
     * @return varchar
     */
    public function getFirstName()
    {
        return $this->getData(self::FIRST_NAME);
    }

    /**
     * Set DC_POSTCODE.
     */
    public function setFirstName($firstName)
    {
        return $this->setData(self::FIRST_NAME, $firstName);
    }

   

}
