<?php

namespace Riverstone\MultiVendor\Model;

use Magento\Framework\Model\AbstractModel;

class Seller extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Riverstone\MultiVendor\Model\Resource\Seller');
    }
}