<?php
namespace Riverstone\MultiVendor\Model\Seller;

use Magento\Framework\Model\AbstractModel;

class SellerRegister extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Riverstone\MultiVendor\Model\ResourceModel\SellerRegister::class);
    }
}
