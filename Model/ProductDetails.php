<?php
namespace Riverstone\MultiVendor\Model;

use Magento\Framework\Model\AbstractModel;

class ProductDetails extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Riverstone\MultiVendor\Model\ResourceModel\ProductDetails::class);
    }
}
