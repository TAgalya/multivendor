<?php
namespace Riverstone\MultiVendor\Model\ResourceModel\Product;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Riverstone\MultiVendor\Model\ProductDetails;
use Riverstone\MultiVendor\Model\ResourceModel\ProductDetails as SellerProductDetails;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(ProductDetails::class, SellerProductDetails::class);
    }
}
