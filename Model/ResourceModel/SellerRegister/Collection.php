<?php

namespace Riverstone\MultiVendor\Model\ResourceModel\SellerRegister;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init(
            'Riverstone\MultiVendor\Model\SellerRegister',
            'Riverstone\MultiVendor\Model\ResourceModel\SellerRegister'
        );
    }
}
