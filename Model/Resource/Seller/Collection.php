<?php

namespace Riverstone\MultiVendor\Model\Resource\Seller;

// use Magento\Framework\Model\Resource\Db\Collection\AbstractCollection;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Riverstone\MultiVendor\Model\Seller',
            'Riverstone\MultiVendor\Model\Resource\Seller'
        );
    }
}