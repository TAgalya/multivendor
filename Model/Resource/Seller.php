<?php

namespace Riverstone\MultiVendor\Model\Resource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Seller extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('vendor_list', 'id');
    }
}