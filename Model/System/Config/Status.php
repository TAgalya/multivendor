<?php

namespace Riverstone\MultiVendor\Model\System\Config;

use Magento\Framework\Option\ArrayInterface;

class Status implements ArrayInterface
{
    const APPROVED  = 'approved';
    const PENDING  = 'pending';
    const DISAPPROVED = 'disapproved';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            self::PENDING => __('Pending'),
            self::APPROVED => __('Approved'),
            self::DISAPPROVED => __('Disapproved')
        ];

        return $options;
    }
}
