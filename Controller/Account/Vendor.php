<?php

namespace Riverstone\MultiVendor\Controller\Account;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Vendor extends Action
{
    protected $resultPageFactory;
    protected $helperData;

    /**
     * @var resultFactory
     */
    protected $resultFactory;

    /**
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Riverstone\MultiVendor\Helper\Data $helperData,
        ResultFactory $resultFactory
    ){
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->helperData = $helperData;
        $this->resultFactory = $resultFactory;
    }

    public function execute()
    {
        if($this->helperData->getModuleStatus()){
            $resultPage = $this->resultPageFactory->create();
            return $resultPage;
        }

        $resultForward = $this->resultFactory->create(ResultFactory::TYPE_FORWARD);
        $resultForward->forward('noroute');
        return $resultForward;

    }
}
