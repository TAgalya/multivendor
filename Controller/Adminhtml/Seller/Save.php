<?php

namespace Riverstone\MultiVendor\Controller\Adminhtml\Seller;


use Magento\Backend\App\Action;
use Magento\Framework\Filesystem;
use Magento\Backend\App\Action\Context;
use Riverstone\MultiVendor\Model\EmailSender;
use Riverstone\MultiVendor\Model\SellerFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\AccountManagement;

class Save extends Action
{

    private $customerFactory;

    /**
     * @var AccountManagement
     */
    private $accountManagement;


    private $storeManager;
    /**
     * @var UploaderFactory
     */
    private $uploaderFactory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Seller model factory
     *
     * @var \Riverstone\MultiVendor\Model\SellerFactory
     */
    protected $_sellerFactory;
    protected $emailSender;

    protected $storeManagerInterface;

    protected $storeRepository;

    public function __construct(
        Context $context,
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem,
        EmailSender $emailSender,
        SellerFactory $sellerFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Api\StoreRepositoryInterface $storeRepository,
        AccountManagement $accountManagement

    ) {
        $this->uploaderFactory = $uploaderFactory;
        $this->filesystem = $filesystem;
        $this->emailSender = $emailSender;
        $this->_sellerFactory = $sellerFactory;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->storeRepository = $storeRepository;
        $this->accountManagement = $accountManagement;
        parent::__construct($context);
    }

   /**
     * @return void
     */
   public function execute()
   {
      $isPost = $this->getRequest()->getPost();

      if ($isPost) {
        $formData = $this->getRequest()->getParam('seller');

         $sellerModel = $this->_sellerFactory->create();
         $sellerId = $this->getRequest()->getParam('id');

         $sellerEmail = '';
         $sellerStatus = '';
         if (isset($formData['id'])) {
            $sellerModel->load($formData['id']);
            $sellerEmail = $sellerModel->getEmail();
            $sellerStatus = $sellerModel->getVendorStatus();
         }

         if(isset($formData['email'])){
            $existingRecord = $sellerModel->getCollection()
            ->addFieldToFilter('email', $formData['email'])
            ->getFirstItem();
   
            if ($existingRecord->getId()) {
                $this->messageManager->addErrorMessage(__('Seller with this email already exists.  Please try to login or create account with diferent email.'));
                $this->_redirect('*/*/');
                return;
            }
         }

         if(isset($formData['store_code'])) {
            $formData['store_code'] = 'default';
         }
         if(isset($formData['banner']['delete'])) {
                $formData['banner'] = '';
         }
         
         if(isset($formData['logo']['delete'])) {
                $formData['logo'] = '';
         }

         if(isset($formData['banner']['value'])) {
            $formData['banner'] = $formData['banner']['value'];
        }
        if(isset($formData['logo']['value'])) {
            $formData['logo'] = $formData['logo']['value'];
        }
         
         if(isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {
            try{
                $uploaderFactories = $this->uploaderFactory->create(['fileId' => 'logo']);
                $uploaderFactories->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploaderFactories->setAllowRenameFiles(true);
                $uploaderFactories->setFilesDispersion(true);
                $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                $destinationPath = $mediaDirectory->getAbsolutePath('vendor/logo/');
                $result = $uploaderFactories->save($destinationPath);
                if (!$result) {
                    throw new LocalizedException(
                        __('File cannot be saved to path: $1', $destinationPath)
                    );
                }
                $formData['logo'] = 'vendor/logo'.$result['file'];
                $formData['logo_url'] = $destinationPath.$result['file'];
                $formData['logo_name'] = $_FILES['logo']['name'] ;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if(isset($_FILES['banner']['name']) && $_FILES['banner']['name'] != '') {
            try{
                $uploaderFactories = $this->uploaderFactory->create(['fileId' => 'banner']);
                $uploaderFactories->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploaderFactories->setAllowRenameFiles(true);
                $uploaderFactories->setFilesDispersion(true);
                $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                $destinationPath = $mediaDirectory->getAbsolutePath('vendor/banner/');
                $result = $uploaderFactories->save($destinationPath);
                if (!$result) {
                    throw new LocalizedException(
                        __('File cannot be saved to path: $1', $destinationPath)
                    );
                }
                $formData['banner'] = 'vendor/banner'.$result['file'];
                $formData['banner_url'] = $destinationPath.$result['file'];
                $formData['banner_name'] = $_FILES['banner']['name'] ;;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

          try {
            $sellerModel->setData($formData);
            // Save seller
            if($sellerModel->save()){
              
                if($sellerEmail){
                    $formData['email'] = $sellerEmail;
                }
                if($sellerStatus == ''){
                    if($formData['vendor_status'] == 'pending'){
                        $this->emailSender->sendEmail($formData, 'seller_registration_email_template');
                        $this->messageManager->addSuccess(__('The seller has been saved.'));
                    }
                    
                    if($formData['vendor_status'] == 'approved'){
                        $createAccount = $this->createAccount($sellerModel);

                        if($createAccount['status'] == 'success'){
                        $resetLink = $this->generatePasswordResetLink($formData['email']);   
                        $formData['resetlink'] = $resetLink;
                        
                        $this->emailSender->sendEmail($formData, 'seller_approved_email_template');
                            $this->messageManager->addSuccess(__($createAccount['message']));
                        }else{
                         $this->messageManager->addError($createAccount['message']);
                        }
                    }

                    if($formData['vendor_status'] == 'disapproved'){
                        $disapproveAccount = $this->disapproveAccount($sellerModel);
                        if($disapproveAccount['status'] == 'success'){
                            $this->emailSender->sendEmail($formData, 'seller_disapproved_email_template');
                            $this->messageManager->addSuccess(__($disapproveAccount['message']));
                        }else{
                         $this->messageManager->addError($disapproveAccount['message']);
                        }
                    }
                }
                if($sellerStatus != '' && $sellerStatus != $formData['vendor_status'] ){

                    if($formData['vendor_status'] == 'approved'){
                     
                       $createAccount = $this->createAccount($sellerModel);
                        if($createAccount['status'] == 'success'){
                            // $resetLink = $this->generatePasswordResetLink($formData['email']);   
                            // print_r($resetLink);
                            // exit();
                            $formData['resetlink'] = '';
                            $this->emailSender->sendEmail($formData, 'seller_approved_email_template');
                            $this->messageManager->addSuccess(__($createAccount['message']));
                        }else{
                         $this->messageManager->addError($createAccount['message']);
                        }
                    }

                    if($formData['vendor_status'] == 'disapproved'){
                 
                       $disapproveAccount = $this->disapproveAccount($sellerModel);
                        if($disapproveAccount['status'] == 'success'){
                            $this->emailSender->sendEmail($formData, 'seller_disapproved_email_template');
                            $this->messageManager->addSuccess(__($disapproveAccount['message']));
                        }else{
                         $this->messageManager->addError($disapproveAccount['message']);
                        }
                    }
                }
            }

            // Check if 'Save and Continue'
            if ($this->getRequest()->getParam('back')) {
               $this->_redirect('*/*/edit', ['id' => $sellerModel->getId(), '_current' => true]);
               return;
            }

            // Go to grid page
            $this->_redirect('*/*/');
            return;
         } catch (\Exception $e) {
            $this->messageManager->addError('224 '.$e->getMessage());
         }

         $this->_getSession()->setFormData($formData);
         $this->_redirect('*/*/edit', ['id' => $sellerId]);
      }
   }

   public function createAccount($sellerModel){
    $sellerModelNew = $this->_sellerFactory->create();
    $sellerModelNew->load($sellerModel->getId());
    // print_r($sellerModelNew->getData());
    // exit();
    try{
        $store = $this->storeRepository->get($sellerModelNew->getStoreCode());
        $websiteId = $store->getWebsiteId();
        $email = $sellerModelNew->getEmail();
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->setStore($store);
        $customer->loadByEmail($email);

        // Check if customer is not exists
        if (!$customer->getId()) {
            $customer = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setStore($store);
            $customer->setEmail($email);
            $customer->setFirstname($sellerModelNew->getFirstName());
            $customer->setLastname($sellerModelNew->getLastName());
            $customer->setIsSeller(1);
        } else if($customer->getId()){
            $customer->setIsSeller(1);
        }
        $customer->save();
        $result['status'] = 'success';
        $result['message'] =  'Seller Account Created Successfully';
        return $result;
    } catch (\Exception $e) {
        $result['status'] = 'error';
        $result['message'] ='test ' .$e->getMessage();
        return $result;
     }
   }


   public function disapproveAccount($sellerModel){
    $sellerModelD = $this->_sellerFactory->create();
    $sellerModelD->load($sellerModel->getId());

    try{
        $store = $this->storeRepository->get($sellerModelD->getStoreCode());
        $websiteId = $store->getWebsiteId();
        $email = $sellerModelD->getEmail();
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->setStore($store);
        $customer->loadByEmail($email);
        if($customer->getId()){
            $customer->setIsSeller(0);
        }
        $customer->save();
        $result['status'] = 'success';
        $result['message'] = 'Seller Account Disapproved';
        return $result;
    } catch (\Exception $e) {
        $result['status'] = 'error';
        $result['message'] = $e->getMessage();
        return $result;
     }
   }

   /**
     * Generate password reset link for a customer by email
     *
     * @param string $customerEmail
     * @return string|null
     */
    public function generatePasswordResetLink($customerEmail)
    {
        // Load customer by email
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($this->storeManager->getStore()->getWebsiteId());
        $customer->loadByEmail($customerEmail);

        if ($customer->getId()) {
            // Generate password reset link
            $resetPasswordLink = $this->accountManagement->initiatePasswordReset(
                $customer->getEmail(),
                AccountManagement::EMAIL_RESET
            );

            return $resetPasswordLink;
        }

        return null;
    }

}