<?php

namespace Riverstone\MultiVendor\Controller\Adminhtml\Seller;

use Riverstone\MultiVendor\Controller\Adminhtml\Seller;

class Delete extends Seller
{
   /**
    * @return void
    */
   public function execute()
   {
      $sellerId = (int) $this->getRequest()->getParam('id');

      if ($sellerId) {
         $sellerModel = $this->_sellerFactory->create();
         $sellerModel->load($sellerId);

         // Check this seller exists or not
         if (!$sellerModel->getId()) {
            $this->messageManager->addError(__('This seller no longer exists.'));
         } else {
               try {
                  // Delete seller
                  $sellerModel->delete();
                  $this->messageManager->addSuccess(__('The seller has been deleted.'));

                  // Redirect to grid page
                  $this->_redirect('*/*/');
                  return;
               } catch (\Exception $e) {
                   $this->messageManager->addError($e->getMessage());
                   $this->_redirect('*/*/edit', ['id' => $sellerModel->getId()]);
               }
            }
      }
   }
}
