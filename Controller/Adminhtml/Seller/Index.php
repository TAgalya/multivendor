<?php

namespace Riverstone\MultiVendor\Controller\Adminhtml\Seller;

use Riverstone\MultiVendor\Controller\Adminhtml\Seller;

class Index extends Seller
{
    /**
     * @return void
     */
   public function execute()
   {
      if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Riverstone_MultiVendor::main_menu');
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Seller'));

        return $resultPage;
   }
}
