<?php

namespace Riverstone\MultiVendor\Controller\Adminhtml\Seller;

use Riverstone\MultiVendor\Controller\Adminhtml\Seller;

class MassDelete extends Seller
{
   /**
    * @return void
    */
   public function execute()
   {
      // Get IDs of the selected seller
      $sellerIds = $this->getRequest()->getParam('seller');

        foreach ($sellerIds as $sellerId) {
            try {
                $sellerModel = $this->_sellerFactory->create();
                $sellerModel->load($sellerId)->delete();
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if (count($sellerIds)) {
            $this->messageManager->addSuccess(
                __('A total of %1 record(s) were deleted.', count($sellerIds))
            );
        }

        $this->_redirect('*/*/index');
   }
}