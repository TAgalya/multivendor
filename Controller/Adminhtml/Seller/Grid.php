<?php

namespace Riverstone\MultiVendor\Controller\Adminhtml\Seller;

use Riverstone\MultiVendor\Controller\Adminhtml\Seller;

class Grid extends Seller
{
   /**
     * @return void
     */
   public function execute()
   {
      return $this->_resultPageFactory->create();
   }
}