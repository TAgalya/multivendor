<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Riverstone\MultiVendor\Controller\Adminhtml\Seller;

use Riverstone\MultiVendor\Controller\Adminhtml\Seller;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Riverstone\MultiVendor\Model\SellerFactory;

class ExportSearchCsv extends Seller
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Result page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Seller model factory
     *
     * @var \Riverstone\MultiVendor\Model\SellerFactory
     */
    protected $sellerFactory;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        SellerFactory $sellerFactory
    ) {
        $this->fileFactory = $fileFactory;
        parent::__construct($context,$coreRegistry, $resultPageFactory, $sellerFactory);
    }

    /**
     * Export search report grid to CSV format
     *
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Exception
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Layout $resultLayout */
        $resultLayout = $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);
        $content = $resultLayout->getLayout()->getChildBlock('riverstone_multivendor_seller.grid.container', 'grid.export');
        return $this->fileFactory->create('search.csv', $content->getCsvFile(), DirectoryList::VAR_DIR);
    }
}
