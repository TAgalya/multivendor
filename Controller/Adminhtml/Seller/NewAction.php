<?php

namespace Riverstone\MultiVendor\Controller\Adminhtml\Seller;

use Riverstone\MultiVendor\Controller\Adminhtml\Seller;

class NewAction extends Seller
{
   /**
     * Create new seller action
     *
     * @return void
     */
   public function execute()
   {
      $this->_forward('edit');
   }
}
