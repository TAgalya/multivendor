<?php

namespace Riverstone\MultiVendor\Controller\Adminhtml\Seller;

use Riverstone\MultiVendor\Controller\Adminhtml\Seller;

class Edit extends Seller
{
   /**
     * @return void
     */
   public function execute()
   {
      $sellerId = $this->getRequest()->getParam('id');
        /** @var \Riverstone\MultiVendor\Model\Seller $model */
        $model = $this->_sellerFactory->create();

        if ($sellerId) {
            $model->load($sellerId);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This seller no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        // Restore previously entered form data from session
        $data = $this->_session->getSellerData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('multivendor_seller', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Riverstone_MultiVendor::main_menu');
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Seller'));

        return $resultPage;
   }
}
