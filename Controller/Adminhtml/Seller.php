<?php

namespace Riverstone\MultiVendor\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Riverstone\MultiVendor\Model\SellerFactory;

class Seller extends Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Result page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Seller model factory
     *
     * @var \Riverstone\MultiVendor\Model\SellerFactory
     */
    protected $_sellerFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param SellerFactory $sellerFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        SellerFactory $sellerFactory
    ) {
       parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_sellerFactory = $sellerFactory;
    }

    /**
     * Seller access rights checking
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Riverstone_MultiVendor::manage_seller');
    }
    public function execute()
    {
    /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
    $resultPage = $this->_resultPageFactory->create();
    
    return $resultPage;
   }
}
