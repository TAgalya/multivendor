<?php
namespace Riverstone\MultiVendor\Controller\Seller;

use Magento\Framework\App\Action\Context;
use Riverstone\MultiVendor\Model\Seller\SellerRegisterFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Riverstone\MultiVendor\Model\EmailSender;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var UploaderFactory
     */
    private $uploaderFactory;

    /**
     * @var Filesystem
     */
    private $filesystem;


    protected $sellerRegisterFactory;

    public function __construct(
        Context $context,
        SellerRegisterFactory $sellerRegisterFactory,
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem,
        EmailSender $emailSender
    ) {
        $this->sellerRegisterFactory = $sellerRegisterFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->filesystem = $filesystem;
        $this->emailSender = $emailSender;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        if (!empty($data)) {
            try {
                $email = $data['email'];

                // Check if email already exists
                $sellerRegisterFactory = $this->sellerRegisterFactory->create();

                $existingRecord = $sellerRegisterFactory->getCollection()
                    ->addFieldToFilter('email', $email)
                    ->getFirstItem();
    
                if ($existingRecord->getId()) {
                    $this->messageManager->addErrorMessage(__('Seller with this email already exists.  Please try to login or create account with other email.'));
                    return $this->_redirect('multivendor/seller/register');
                }

                $attachments = [];
                if(isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {
                    try{
                        $uploaderFactories = $this->uploaderFactory->create(['fileId' => 'logo']);
                        $uploaderFactories->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $uploaderFactories->setAllowRenameFiles(true);
                        $uploaderFactories->setFilesDispersion(true);
                        $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                        $destinationPath = $mediaDirectory->getAbsolutePath('vendor/logo/');
                        $result = $uploaderFactories->save($destinationPath);
                        if (!$result) {
                            throw new LocalizedException(
                                __('File cannot be saved to path: $1', $destinationPath)
                            );
                        }
                        $data['logo'] = 'vendor/logo'.$result['file'];
                        $data['logo_url'] = $destinationPath.$result['file'];
                        $data['logo_name'] = $_FILES['logo']['name'] ;
                    } catch (\Exception $e) {
                        
                    }
                }
                if(isset($_FILES['banner']['name']) && $_FILES['banner']['name'] != '') {
                    try{
                        $uploaderFactories = $this->uploaderFactory->create(['fileId' => 'banner']);
                        $uploaderFactories->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $uploaderFactories->setAllowRenameFiles(true);
                        $uploaderFactories->setFilesDispersion(true);
                        $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                        $destinationPath = $mediaDirectory->getAbsolutePath('vendor/banner/');
                        $result = $uploaderFactories->save($destinationPath);
                        if (!$result) {
                            throw new LocalizedException(
                                __('File cannot be saved to path: $1', $destinationPath)
                            );
                        }
                        $data['banner'] = 'vendor/banner'.$result['file'];
                        $data['banner_url'] = $destinationPath.$result['file'];
                        $data['banner_name'] = $_FILES['banner']['name'] ;;
                    } catch (\Exception $e) {
                        
                    }
                }

                $sellerRegisterFactory->setData($data)->save();

                $this->emailSender->sendEmail($data, 'seller_registration_email_template');

                $this->messageManager->addSuccessMessage(__('Registration successful.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('An error occurred while registering.'. $e));
            }
        }

        $this->_redirect('multivendor/seller/register');
    }
}
