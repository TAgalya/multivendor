<?php

namespace Riverstone\MultiVendor\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Riverstone\MultiVendor\Model\ProductDetailsFactory;
use Riverstone\MultiVendor\Model\ResourceModel\ProductDetails;

class Save extends Action
{
    protected $productDetailsFactory;
    protected $resourceModel;

    public function __construct(
        Context                          $context,
        ProductDetailsFactory        $productDetailsFactory,
        ProductDetails $resourceModel
    )
    {
        $this->productDetailsFactory = $productDetailsFactory;
        $this->resourceModel = $resourceModel;
        return parent::__construct($context);
    }


    public function execute()
    {

        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getParams();
        // print_r($data);
        // exit();
        $modelFactory = $this->productDetailsFactory->create();
        $modelFactory->setData($data);
        try {
            $this->resourceModel->save($modelFactory);
            $this->messageManager->addSuccessMessage(__('You saved the product details.'));
            return $resultRedirect->setPath('multivendor/account/add');
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $resultRedirect->setPath('multivendor/account/add');
        }
        print_r($data);
        exit();
    }
}
