<?php

namespace Riverstone\MultiVendor\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use Riverstone\MultiVendor\Model\ResourceModel\SellerRegister\CollectionFactory as CustomerCollectionFactory;


class Search extends Action
{

    protected $pageFactory;
    protected $customerCollectionFactory;

    public function __construct(
        Context                   $context,
        PageFactory               $pageFactory,
        CustomerCollectionFactory $customerCollectionFactory
    )
    {
        $this->pageFactory = $pageFactory;
        $this->customerCollectionFactory = $customerCollectionFactory;
        parent::__construct($context);
    }

    /**
     * @throws NotFoundException
     */
    public function execute()
    {
        $query = $this->getRequest()->getParam('q');

        $customerCollection = $this->customerCollectionFactory->create();
        $customerCollection->addFieldToSelect('*');
        $customerCollection->addFieldToFilter( $query
//            [
//                ['attribute' => 'firstname', 'like' => '%' . $query . '%'],
//                ['attribute' => 'lastname', 'like' => '%' . $query . '%'],
//                ['attribute' => 'address', 'like' => '%' . $query . '%']
//            ]
        );
        $customerCollection->setPageSize(10);
        $this->getRequest()->setParams(['searchResults' => $customerCollection]);
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
