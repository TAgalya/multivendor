define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'jquery/ui'
], function ($, modal) {
    'use strict';

    window.aureatelabsView = {
        openPanel: function (element) {
            var panelTemplate = $('#sliding-panel-template').html();
            var panelContainer = $('<div/>').addClass('sliding-panel-container');
            var panelContent = $('<div/>').addClass('sliding-panel-content').html(panelTemplate);
            panelContainer.append(panelContent);
            $(element).closest('tr').after(panelContainer);
            panelContainer.slideDown();
        }
    };

    // return function (config) {
    //     var options = {
    //         type: 'slide',
    //         responsive: true,
    //         innerScroll: true,
    //         buttons: [{
    //             text: $.mage.__('Close'),
    //             class: '',
    //             click: function () {
    //                 this.closeModal();
    //             }
    //         }]
    //     };

    //     var popup = modal(options, $('#edit_form_slider'));
    //     var triggerSelector = config.triggerSelector;
    //     var formContainer = config.formContainer;

    //     $(triggerSelector).on('click', function () {
    //         var rowData = $(this).closest('tr').data();
    //         var formUrl = config.formUrl + 'id/' + rowData.id;

    //         $.ajax({
    //             url: formUrl,
    //             type: 'get',
    //             dataType: 'html',
    //             success: function (data) {
    //                 $(formContainer).html(data);
    //                 popup.modal('openModal');
    //             },
    //             error: function () {
    //                 console.error('Error occurred while fetching the form content.');
    //             }
    //         });
    //     });
    // };
});
