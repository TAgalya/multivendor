<?php
namespace Riverstone\MultiVendor\Block\Seller;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Data\Form\FormKey;

class SellerRegister extends Template 
{
    protected $formKey;

    protected $storeManager;

    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        FormKey $formKey,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->formKey = $formKey;
        parent::__construct($context, $data);
    }

    public function getFormAction()
    {
        return $this->getUrl('multivendor/seller/save', ['_secure' => true]);
    }

    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
    public function getStoreCode()
    {
        return $this->storeManager->getStore()->getCode();

    }
    public function getWebsiteId(){
        return $this->storeManager->getStore()->getWebsiteId();
    }
}
