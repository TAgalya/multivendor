<?php

namespace Riverstone\MultiVendor\Block\Product;

use Magento\Catalog\Helper\ImageFactory;
use Magento\Eav\Api\AttributeSetRepositoryInterface;

class ProductCollection extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;
    protected $imageHelperFactory;
    protected $attributeSetRepository;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        ImageFactory $imageHelperFactory,
        AttributeSetRepositoryInterface $attributeSetRepository,
        array $data = [])
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->imageHelperFactory = $imageHelperFactory;
        $this->attributeSetRepository = $attributeSetRepository;
        parent::__construct($context, $data);
    }

    public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        return $collection;
    }
    public function getImageHelper()
    {
        return $this->imageHelperFactory->create();
    }
    public function getAttributeSetRepository()
    {
        return $this->attributeSetRepository;
    }

}
