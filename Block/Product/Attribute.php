<?php

namespace Riverstone\MultiVendor\Block\Product;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;

class Attribute extends Template
{
    protected $collectionFactory;
protected $attributeManagement;
protected $productAttributeRepository;
protected $repository;


    public function __construct(
        Context   $context,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface         $storeManager,
        CollectionFactory                                  $collectionFactory,
        \Magento\Catalog\Api\ProductAttributeManagementInterface $attributeManagement,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $productAttributeRepository,
        \Magento\Catalog\Model\Product\Attribute\Repository $repository,
        array                                              $data = []
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->collectionFactory = $collectionFactory;
        $this->attributeManagement= $attributeManagement;
        $this->productAttributeRepository=$productAttributeRepository;
        $this->repository=$repository;
        parent::__construct($context, $data);
    }

    public function getAttribute()
    {
//        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/custom.log');
//        $logger = new \Zend_Log();
//        $logger->addWriter($writer);
//
//        $attributeSetCollection = $this->collectionFactory->create();
//        $attributeSetCollection->addFieldToFilter('4', Product::ENTITY);
//
//        $attributeSetList = [];
//        foreach ($attributeSetCollection as $attributeSet) {
//            $attributeSetList[] = [
//                'name' => $attributeSet->getAttributeSetName()
//            ];
//        }
//        $logger->info(print_r($attributeSetCollection->getItems(), true));
//        return $attributeSetList;


        $productAttributesManagement = $this->attributeManagement;
//        $productAttributesRepository = $objectManager->get('\Magento\Catalog\Api\ProductAttributeRepositoryInterface');
//        $attrubuteOptionRepository = $objectManager->get('\Magento\Catalog\Model\Product\Attribute\Repository');

        $productAttributes = $productAttributesManagement->getAttributes(4);

        print_r($productAttributes);
        exit();
    }

}
