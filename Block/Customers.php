<?php

namespace Riverstone\MultiVendor\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Riverstone\MultiVendor\Model\SellerRegisterFactory ;
class Customers extends Template
{
    protected $gridFactory;

    public function __construct(
        Context       $context,
        SellerRegisterFactory $gridFactory,
        array         $data = []
    )
    {
        $this->gridFactory = $gridFactory;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $collection = $this->gridFactory->create()->getCollection();
        $this->setCollection($collection);
        $this->pageConfig->getTitle()->set(__('Customer List'));
        if ($this->getCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'webkul.grid.record.pager'
            )->setCollection(
                $this->getCollection()
            );
            $this->setChild('pager', $pager);
        }
        return $this;
    }

    /**
     * @return string
     */
    // method for get pager html
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
