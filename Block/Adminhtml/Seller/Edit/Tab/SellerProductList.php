<?php
namespace Riverstone\MultiVendor\Block\Adminhtml\Seller\Edit\Tab;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Framework\Registry;
use Magento\Catalog\Model\ProductFactory;

class SellerProductList extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_coreRegistry = null;

    protected $_collectionFactory;

    public function __construct(
        Context $context,
        Data $backendHelper,
        ProductFactory $productFactory,
        Registry $coreRegistry,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_collectionFactory = $productFactory;
        parent::__construct($context, $backendHelper, $data);
    }

protected function _construct()
{
    parent::_construct();
    $this->setId('view_custom_grid');
    $this->setDefaultSort('created_at', 'desc');
    $this->setSortable(false);
    $this->setPagerVisibility(false);
    $this->setFilterVisibility(false);
}

protected function _prepareCollection()
{
    $collection = $this->_collectionFactory->create()->setCustomerId(
        $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID));
    $this->setCollection($collection);
    return parent::_prepareCollection();
}


protected function _prepareColumns()
{
    $this->addColumn(
        'product_id',
        ['header' => __('ID'), 'index' => 'product_id', 'type' => 'number', 'width' => '100px']
    );
    $this->addColumn(
        'product_name',
        [
            'header' => __('Product Name'),
            'index' => 'name',
        ]
    );
    return parent::_prepareColumns();
}

public function getHeadersVisibility()
{
    return $this->getCollection()->getSize() >= 0;
}

public function getRowUrl($row)
{
    return $this->getUrl('catalog/product/edit', ['id' => $row->getProductId()]);
}
}

// use Magento\Backend\Block\Template;
// use Riverstone\MultiVendor\Model\SellerFactory;
// use Magento\Backend\Block\Widget\Grid\Extended;
// use Magento\Framework\UrlInterface;

// class SellerProductList extends Extended
// {
//     protected $collectionFactory;

//     protected $urlBuilder;
//     public function __construct(
//         \Magento\Backend\Block\Template\Context $context,
//         \Magento\Backend\Helper\Data $backendHelper,
//         UrlInterface $urlBuilder,
//         SellerFactory $collectionFactory,
//         array $data = []
//     ) {
//         $this->urlBuilder = $urlBuilder;
//         $this->collectionFactory = $collectionFactory;
//         parent::__construct($context, $backendHelper, $data);
//     }

//     protected function _construct()
//     {
//         parent::_construct();
//         $this->setDefaultSort('id');
//         $this->setDefaultDir('ASC');
//         $this->setSaveParametersInSession(true);
//         $this->setUseAjax(true);
//     }


//     protected function _prepareCollection()
//     {
//         $collection = $this->collectionFactory->create()->getCollection();
//         $this->setCollection($collection);
//         return parent::_prepareCollection();
//     }

//     protected function _prepareColumns()
//     {
//             // Add Image Column
//         $this->addColumn(
//             'logo',
//             [
//                 'header' => __('Logo'),
//                 'index' => 'logo',
//                 'renderer' => 'Riverstone\MultiVendor\Block\Adminhtml\Widget\Grid\Column\Renderer\Image',
//             ]
//         );

//         $this->addColumn(
//             'id',
//             [
//                 'header' => __('ID'),
//                 'index' => 'id',
//                 'type' => 'number'
//             ]
//         );

//         $this->addColumn(
//             'first_name',
//             [
//                 'header' => __('Seller First Name'),
//                 'index' => 'first_name',
//             ]
//         );

//         $this->addColumn(
//             'last_name',
//             [
//                 'header' => __('Seller Last Name'),
//                 'index' => 'last_name',
//             ]
//         );

//         $this->addColumn(
//             'email',
//             [
//                 'header' => __('Email'),
//                 'index' => 'email',
//             ]
//         );

//         $this->addColumn(
//             'shop_name',
//             [
//                 'header' => __('Shop Name'),
//                 'index' => 'shop_name',
//             ]
//         );

//             // Add Action Column
//     $this->addColumn(
//         'action',
//         [
//             'header' => __('Action'),
//             'type' => 'action',
//             'getter' => 'getId',
//             'actions' => [
//                 [
//                     'caption' => __('Edit'),
//                     'field' => 'id',
//                     'onclick' => 'aureatelabsView.openPanel(this)',
//                     'class' => 'aureatelabs-open-panel'
//                 ],
//                 [
//                     'caption' => __('Delete'),
//                     'url' => ['base' => '*/*/delete'],
//                     'field' => 'id',
//                     'confirm' => __('Are you sure you want to delete this record?')
//                 ]
//             ],
//             'filter' => false,
//             'sortable' => false,
//             'index' => 'stores',
//             'header_css_class' => 'col-actions',
//             'column_css_class' => 'col-actions'
//         ]
//     );

//     // $this->addColumn(
//     //     'action',
//     //     [
//     //         'header' => __('Action'),
//     //         'type' => 'action',
//     //         'getter' => 'getId',
//     //         'actions' => [
//     //             [
//     //                 'caption' => __('Edit'),
//     //                 'url' => '#',
//     //                 'field' => 'id',
//     //                 'popup' => [
//     //                     'triggerSelector' => '.edit-action',
//     //                     'openOnRowClick' => true,
//     //                     'size' => 'lg',
//     //                     'formUrl' => $this->getUrl('multivendor/seller/edit'),
//     //                     'formContainer' => '#edit_form_slider',
//     //                     'beforeFormOpen' => 'function() { $("#edit_form_slider").modal("openModal"); }',
//     //                     'afterFormClose' => 'function() { $("#edit_form_slider").modal("closeModal"); }',
//     //             ],
//     //             [
//     //                 'caption' => __('Delete'),
//     //                 'url' => ['base' => '*/*/delete'],
//     //                 'field' => 'id',
//     //                 'confirm' => __('Are you sure you want to delete this record?')
//     //             ]
//     //         ],
//     //         'filter' => false,
//     //         'sortable' => false,
//     //         'index' => 'stores',
//     //         'header_css_class' => 'col-actions',
//     //         'column_css_class' => 'col-actions'
//     //     ]
//     //     ]
//     // );

//     // $this->addColumn(
//     //     'action',
//     //     [
//     //         'header' => __('Action'),
//     //         'type' => 'action',
//     //         'getter' => 'getId',
//     //         'actions' => [
//     //             [
//     //                 'caption' => __('Edit'), // Make sure this line is present
//     //                 'url' => '#',
//     //                 'field' => 'id',
//     //                 'popup' => [
//     //                     'triggerSelector' => '.edit-action',
//     //                     'openOnRowClick' => true,
//     //                     'size' => 'lg',
//     //                     'formUrl' => $this->getUrl('multivendor/seller/edit'),
//     //                     'formContainer' => '#edit_form_slider',
//     //                     'beforeFormOpen' => 'function() { $("#edit_form_slider").modal("openModal"); }',
//     //                     'afterFormClose' => 'function() { $("#edit_form_slider").modal("closeModal"); }',
//     //                 ],
//     //             ],
//     //             // ...
//     //         ],
//     //         // ...
//     //     ]
//     // );
  

//         // Add more columns as needed

//         return parent::_prepareColumns();
//     }

//     public function getViewUrl()
//     {
//         return $this->urlBuilder->getUrl(
//             $this->getData('multivendor/grid/view')
//         );
//     }

//     /**
//      * Prepare label for tab
//      *
//      * @return string
//      */
//     public function getTabLabel()
//     {
//         return __('Seller Info');
//     }
 
//     /**
//      * Prepare title for tab
//      *
//      * @return string
//      */
//     public function getTabTitle()
//     {
//         return __('Seller Info');
//     }
 
//     /**
//      * {@inheritdoc}
//      */
//     public function canShowTab()
//     {
//         return true;
//     }
 
//     /**
//      * {@inheritdoc}
//      */
//     public function isHidden()
//     {
//         return false;
//     }
// }


