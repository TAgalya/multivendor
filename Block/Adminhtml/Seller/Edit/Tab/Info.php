<?php

namespace Riverstone\MultiVendor\Block\Adminhtml\Seller\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;
use Riverstone\MultiVendor\Model\System\Config\Status;

class Info extends Generic implements TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Riverstone\MultiVendor\Model\Config\Status
     */
    protected $_sellerStatus;

   /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Config $wysiwygConfig
     * @param Status $sellerStatus
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        Status $sellerStatus,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_sellerStatus = $sellerStatus;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
       /** @var $model \Riverstone\MultiVendor\Model\Seller */
        $model = $this->_coreRegistry->registry('multivendor_seller');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('seller_');
        $form->setFieldNameSuffix('seller');
        $form->setEnctype('multipart/form-data');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General')]
        );
        $disabled = false;
        if ($model->getId()) {
            $fieldset->addField(
                'id',
                'hidden',
                ['name' => 'id']
            );
            $disabled = true;         
            $fieldset->addField(
                'store_code',
                'hidden',
                ['name' => 'store_code']
            );
        }else{
            $fieldset->addField(
                'store_code',
                'hidden',
                [
                    'name' => 'store_code',
                    'value' => 'default',
                ]
            );
        }
        $fieldset->addField(
            'first_name',
            'text',
            [
                'name'        => 'first_name',
                'label'    => __('Seller First Name'),
                'required'     => true
            ]
        );
        $fieldset->addField(
            'last_name',
            'text',
            [
                'name'        => 'last_name',
                'label'    => __('Seller Last Name'),
                'required'     => true
            ]
        );
        $fieldset->addField(
            'email',
            'text',
            [
                'name'        => 'email',
                'label'    => __('Email'),  
                'required'     => true,
                'disabled' => $disabled
            ]
        );
        $fieldset->addField(
            'shop_name',
            'text',
            [
                'name'        => 'shop_name',
                'label'    => __('Shop Name'),
                'required'     => true
            ]
        );
        $fieldset->addField(
            'shop_url',
            'text',
            [
                'name'        => 'shop_url',
                'label'    => __('Shop URL'),
                'required'     => false
            ]
        );
        $wysiwygConfig = $this->_wysiwygConfig->getConfig();
        $fieldset->addField(
            'shop_description',
            'editor',
            [
                'name'        => 'shop_description',
                'label'    => __('Shop Description'),
                'required'     => true,
                'config'    => $wysiwygConfig
            ]
        );
        $fieldset->addField(
            'logo',
            'image',
            [
                'name' => 'logo',
                'label' => __('Logo'),
                'title' => __('Logo'),
                'required'  => false,
            ]
        );
        $fieldset->addField(
            'banner',
            'image',
            [
                'name' => 'banner',
                'label' => __('Shop Banner'),
                'title' => __('Shop Banner'),
                'required'  => false,
            ]
        );
        $fieldset->addField(
            'phone',
            'text',
            [
                'name'        => 'phone',
                'label'    => __('Phone'),
                'required'     => true
            ]
        );
        $fieldset->addField(
            'vendor_status',
            'select',
            [
                'name'      => 'vendor_status',
                'label'     => __('Status'),
                'options'   => $this->_sellerStatus->toOptionArray()
            ]
        );
        $fieldset->addField(
            'meta_keyword',
            'text',
            [
                'name'        => 'meta_keyword',
                'label'    => __('Meta Keyword'),
                'required'     => false
            ]
        );
        $fieldset->addField(
            'meta_description',
            'textarea',
            [
                'name'      => 'meta_description',
                'label'     => __('Meta Description'),
                'required'  => false,
                'style'     => 'height: 2em; width: 30em;'
            ]
        );
        $fieldset->addField(
            'address',
            'textarea',
            [
                'name'      => 'address',
                'label'     => __('Address'),
                'required'  => true,
                'style'     => 'height: 2em; width: 30em;'
            ]
        );
        if ($model->getData('logo')) {
            $logo = $model->getData('logo');
            $fieldset->addField(
                'existing_logo',
                'hidden',
                [
                    'name' => 'existing_logo',
                    'value' => $logo,
                ]
            );
        }
    


        $data = $model->getData();
        
        $form->setValues($data);
        // $form->setUseContainer(true);
        $this->setForm($form);
        // echo $form->toHtml();
        // exit();
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Seller Info');
    }
 
    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Seller Info');
    }
 
    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }
 
    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
