<?php

namespace Riverstone\MultiVendor\Block\Adminhtml\Seller\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs
{
    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('seller_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Seller Information'));
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'seller_info',
            [
                'label' => __('Seller Profile'),
                'title' => __('Seller Profile'),
                'content' => $this->getLayout()
                ->createBlock('Riverstone\MultiVendor\Block\Adminhtml\Seller\Edit\Tab\Info')
                ->toHtml(),
                'active' => true
            ]
        );
        $this->addTab(
            'seller_info_new',
            [
                'label' => __('Seller Products'),
                'title' => __('Seller Products'),
                'content' => $this->getLayout()
                ->createBlock('Riverstone\MultiVendor\Block\Adminhtml\Seller\Edit\Tab\SellerProductList')
                ->toHtml(),
                'active' => false
            ]
        );
        

        return parent::_beforeToHtml();
    }
}
