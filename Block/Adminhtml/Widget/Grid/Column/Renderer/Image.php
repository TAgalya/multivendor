<?php
namespace Riverstone\MultiVendor\Block\Adminhtml\Widget\Grid\Column\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class Image extends AbstractRenderer
{
    public function render(DataObject $row)
    {
        $imageUrl = $this->_getValue($row); // Get the value of the 'image' field from the row data
        
        if (!empty($imageUrl)) {
            return '<img src="http://river.magento.com/pub/media/' . $imageUrl . '" alt="Image" width="50" height="50" />';
        }
        
        return '';
    }
}
