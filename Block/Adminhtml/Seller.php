<?php

namespace Riverstone\MultiVendor\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Seller extends Container
{
   /**
     * Constructor
     *
     * @return void
     */
   protected function _construct()
    {
        $this->_controller = 'adminhtml_seller';
        $this->_blockGroup = 'Riverstone_MultiVendor';
        $this->_headerText = __('Manage Seller');
        $this->_addButtonLabel = __('Add Seller');
        parent::_construct();
    }
}