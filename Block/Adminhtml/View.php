<?php
namespace Riverstone\MultiVendor\Block\Adminhtml;

use Magento\Backend\Block\Template;

class View extends Template
{
   public $_template = 'Riverstone_MultiVendor::seller/view.phtml';

     public function __construct(
       \Magento\Backend\Block\Template\Context $context
   ) {
       parent::__construct($context);
   }
}