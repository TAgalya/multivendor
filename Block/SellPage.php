<?php
namespace Riverstone\MultiVendor\Block;

class SellPage extends \Magento\Framework\View\Element\Template
{   
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Recipient email config path
     */
    public $storeManager;

    const XML_PATH_PAGE_TITLE = 'multi_vendor_section/sell_page/page_title';
    const XML_PATH_BANNER = 'multi_vendor_section/sell_page/banner';
    const XML_PATH_DESCRIPTION = 'multi_vendor_section/sell_page/description';
    const XML_PATH_ABOUT_MULTIVENDOR = 'multi_vendor_section/sell_page/about_multivendor';
    const XML_PATH_THEME_COLOR = 'multi_vendor_section/sell_page/theme_color';

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ){
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    public function getMediaUrl(){
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'helloworld/';
    }

    public function getBaseUrl(){
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK);
    }

    public function getPageTitle()   
    {   
        return $this->scopeConfig->getValue(self::XML_PATH_PAGE_TITLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);   
    }  

    public function getBannerPath()   
    {   
        return $this->scopeConfig->getValue(self::XML_PATH_BANNER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);   
    }  
    
    public function getDescription()   
    {   
        return $this->scopeConfig->getValue(self::XML_PATH_DESCRIPTION, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);   
    }  

    public function getAboutMultivendor()   
    {   
        return $this->scopeConfig->getValue(self::XML_PATH_ABOUT_MULTIVENDOR, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);   
    }  

    public function getThemeColor()   
    {   
        return $this->scopeConfig->getValue(self::XML_PATH_THEME_COLOR, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);   
    }  
}
