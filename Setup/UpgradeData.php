<?php
namespace Riverstone\MultiVendor\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavEntityAttribute;
use Magento\Store\Api\StoreRepositoryInterface;

/**
 * @codeCoverageIgnore
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;
    
    /**
     * @var IndexerRegistry
     */
    protected $indexerRegistry;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig; 
   
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @var EavEntityAttribute
     */
    private $eavEntityAttribute;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepositoryInterface;

    private $eavSetupFactory;

    /**
     * @param CustomerSetupFactory $customerSetupFactory
     * @param IndexerRegistry $indexerRegistry
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param EavEntityAttribute $eavEntityAttribute
     * @param StoreRepositoryInterface $storeRepositoryInterface
     */
    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        IndexerRegistry $indexerRegistry,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory,
        EavEntityAttribute $eavEntityAttribute,
        StoreRepositoryInterface $storeRepositoryInterface,
        EavSetupFactory $eavSetupFactory
        
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->indexerRegistry = $indexerRegistry;
        $this->eavConfig = $eavConfig;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->eavEntityAttribute = $eavEntityAttribute;
        $this->storeRepositoryInterface = $storeRepositoryInterface;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();

            /** @var $attributeSet AttributeSet */
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            /**
             * Create customer attribute not_override_customer_group
             */
            $customerSetup->addAttribute(Customer::ENTITY, 'is_seller',
            [
                'type' => 'int',
                'label' => 'Is Seller',
                'input' => 'boolean',
                'required' => false,
                'system' => false,
                'visible' => true,
                'user_defined' => true,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_searchable_in_grid' => true,
                'is_filterable_in_grid' => true,
                'sort_order' => 210,
                'position' => 210,
                'system' => false,
                'default' => 0,
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
            ]);
            $override_cus = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, "is_seller")
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer'],
                ]);
            $override_cus->save();
        }

        $indexer = $this->indexerRegistry->get(Customer::CUSTOMER_GRID_INDEXER_ID);
        $indexer->reindexAll();
        $this->eavConfig->clear();


        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
 
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'seller',
            [
                'group' => 'General',
                'label' => 'Seller',
                'type'  => 'text',
                'input' => 'multiselect',
                'source' => 'Riverstone\MultiVendor\Model\Config\Product\SellerList',
                'required' => false,
                'sort_order' => 30,
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_STORE,
                'used_in_product_listing' => true,
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'visible_on_front' => false
            ]
        );
    }

    // $ATTRIBUTE_GROUP = 'General'; // Attribute Group Name
    // $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
    // $allAttributeSetIds = $eavSetup->getAllAttributeSetIds($entityTypeId);
    // foreach ($allAttributeSetIds as $attributeSetId) {
    //     $groupId = $eavSetup->getAttributeGroupId($entityTypeId, $attributeSetId, $ATTRIBUTE_GROUP);
    //     $eavSetup->addAttributeToGroup(
    //         $entityTypeId,
    //         $attributeSetId,
    //         $groupId,
    //         'seller',
    //         null
    //     );
    // }
       $setup->endSetup();

    }
}